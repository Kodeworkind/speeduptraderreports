﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Reports.DAL;


namespace Reports.Controllers
{
    public class AdminController : Controller
    {
        IConfiguration _iconfiguration;
        private readonly ReportData reportData;

        public AdminController(IConfiguration configuration)
        {
            _iconfiguration = configuration;
            reportData = new ReportData(configuration);
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("Login")]
        public ActionResult Login(User user)
        {
            if (user.Username != "" && user.Password != null)
            {
                string password = Common.GenerateMD5HashCode(user.Password);
                var obj = reportData.AuthenticateUser(user.Username, password);

                if (obj != null)
                {
                    TempData["userId"] = Convert.ToInt32(obj.UserId);
                    TempData.Keep();
                    return Json("true");
                }
                else
                {
                    return Json("false");
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            TempData["userId"] = 0;
            return Json("true");
        }
    }
}