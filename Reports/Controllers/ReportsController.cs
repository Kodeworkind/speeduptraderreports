﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Reports.DAL;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.ServiceProcess;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Reports.Controllers
{
    public class ReportsController : Controller
    {
        IConfiguration _iconfiguration;
        private readonly ReportData reportData;
        ILogger<ReportsController> _logger;

        public ReportsController(IConfiguration configuration, ILogger<ReportsController> logger)
        {
            _iconfiguration = configuration;
            reportData = new ReportData(configuration);
            _logger = logger;
        }
        public ActionResult Index()
        {
            var sessUserId = Convert.ToInt32(TempData["userId"]);
            if (sessUserId != 0)
            {
                FilterData filter = FilteredData();
                ViewBag.FromDate = filter.FromDate.ToString("dd/MM/yyy", CultureInfo.InvariantCulture);
                ViewBag.ToDate = filter.ToDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                FileStatusModel viewModel = new FileStatusModel();
                viewModel.ListFileStatus = reportData.GetFileStatus();
                TempData["userId"] = sessUserId;
                TempData.Keep();

                List<Service> serviceNames = new List<Service>();
                serviceNames = reportData.GetServiceNames();
                ViewBag.ServiceName = serviceNames;
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
                
        }
        public FilterData FilteredData()
        {
            FilterData filter = new FilterData
            {
                FromDate = DateTime.Today.AddMonths(-1),
                ToDate = DateTime.Today
            };

            return filter;
        }
        #region Search Report
        public JsonResult ReportsSearchResult(FilterData data)
        {
            try
            {
                var sessUserId = Convert.ToInt32(TempData["userId"]);
                if (sessUserId != 0)
                {
                    List<Report> lstReport;
                    ReportDetails countDetails;

                    //if (data.FromDate == data.ToDate)
                    //{
                    //    data.ToDate = data.ToDate.AddDays(1);
                    //}

                    SearchDetails searchDetails = new SearchDetails
                    {
                        Category = data.Category,
                        Status = data.Status,
                        FromDate = data.FromDate,
                        ToDate = data.ToDate
                    };

                    TempData["CategoryId"] = data.Category;
                    TempData["StatusId"] = data.Status;
                    TempData["FromDate"] = Convert.ToDateTime(data.FromDate);
                    TempData["ToDate"] = Convert.ToDateTime(data.ToDate);
                    TempData["userId"] = sessUserId;
                    TempData.Keep();

                    countDetails = reportData.GetCounts(searchDetails);

                    List<Column> columns;
                    if (data.Category != (int)MainCategory.Pending_Emails)
                    {
                        columns = new List<Column>
                        {
                            new Column{Data = "userName", Name = "UserName"},
                            new Column{Data = "csvName", Name = "CsvName"},
                            new Column{Data = "createdOn", Name = "CreatedOn"},
                            new Column{Data = "updatedOn", Name = "UpdatedOn"},
                            new Column{Data = "statusName", Name = "StatusName"}
                        };
                    }
                    else
                    {
                        columns = new List<Column>
                        {
                            new Column{Data = "toEmail", Name = "ToEmail"},
                            new Column{Data = "subject", Name = "Subject"},
                            new Column{Data = "createdOn", Name = "CreatedOn"},
                            new Column{Data = "updatedOn", Name = "UpdatedOn"},
                            new Column{Data = "emailType", Name = "EmailType"}
                        };
                    }

                    lstReport = reportData.GetReportData(searchDetails);
                    var Jdata = new
                    {
                        data = lstReport,
                        columns,
                        countDetails
                    };
                    return Json(new { success = "true", Jdata });
                }
                else
                {
                    return Json(false);
                }
            }
            catch(Exception ex)
            {
                _logger.LogInformation(ex.Message + ex.StackTrace);
                return null;
            }
        } 
        #endregion
        #region Download Excel
        public ActionResult Excel()
        {
            try
            {
                var categoryId = Convert.ToInt32(TempData["CategoryId"]);

                SearchDetails searchDetails = new SearchDetails
                {
                    Category = categoryId,
                    Status = Convert.ToInt32(TempData["StatusId"]),
                    FromDate = Convert.ToDateTime(TempData["FromDate"]),
                    ToDate = Convert.ToDateTime(TempData["ToDate"])
                };

                string[] columnHeaders;

                if (categoryId == (int)MainCategory.Pending_Emails)
                {
                    columnHeaders = new string[]
                    {
                    "Email To",
                    "Subject",
                    "Email Type",
                    "Created On",
                    "Updated On"
                    };
                } else
                {
                    columnHeaders = new string[]
                    {
                    "User Name",
                    "CSV Name",
                    "Created On",
                    "Status",
                    "Updated On"
                    };

                }
                byte[] result;

                using (var package = new ExcelPackage())
                {
                    // add a new worksheet to the empty workbook
                    var worksheet = package.Workbook.Worksheets.Add("Current Employee"); //Worksheet name
                    using (var cells = worksheet.Cells[1, 1, 1, 5])
                    {
                        cells.Style.Font.Bold = true;
                    }
                    //First add the headers
                    for (var i = 0; i < columnHeaders.Count(); i++)
                    {
                        worksheet.Cells[1, i + 1].Value = columnHeaders[i];
                    }
                    //Add values
                    var j = 2;
                    foreach (var employee in reportData.GetReportData(searchDetails))
                    {
                        if (categoryId > (int)MainCategory.Pending_Emails)
                        {
                            worksheet.Cells["A" + j].Value = employee.UserName;
                            worksheet.Cells["B" + j].Value = employee.CsvName;
                            worksheet.Cells["C" + j].Value = employee.CreatedOn;
                            worksheet.Cells["D" + j].Value = employee.StatusName;
                            worksheet.Cells["E" + j].Value = employee.UpdatedOn;
                        }
                        else
                        {
                            worksheet.Cells["A" + j].Value = employee.ToEmail;
                            worksheet.Cells["B" + j].Value = employee.Subject;
                            worksheet.Cells["C" + j].Value = employee.EmailType;
                            worksheet.Cells["D" + j].Value = employee.CreatedOn;
                            worksheet.Cells["E" + j].Value = employee.UpdatedOn;
                        }

                        j++;
                    }
                    worksheet.Cells.AutoFitColumns();
                    worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    result = package.GetAsByteArray();
                }

                string fileName= "";

                switch (categoryId)
                {
                    case (int)MainCategory.Pending_Emails:
                        fileName = "Pending Emails.xlsx";
                        break;
                    case (int)MainCategory.Users:
                        fileName = "Pending Users.xlsx";
                        break;
                    case (int)MainCategory.Account:
                        fileName = "Pending Accounts.xlsx";
                        break;
                    case (int)MainCategory.Rms:
                        fileName = "Pending RMS.xlsx";
                        break;
                }

                return File(result, "application/octet-stream", fileName);
            }
            catch(Exception ex)
            {
                _logger.LogInformation(ex.Message + ex.StackTrace);
                return null;
            }
        } 
        #endregion
        #region Restart Windows Service
        public JsonResult ServiceOperations(string serviceName)
        {
            if(!ServiceExists(serviceName))
                return this.Json(new { success = true, message = "false" });
            
            string result;
            result = RestartService(serviceName, 10000);
            _logger.LogInformation(result);
           
            if (result != "")
                return this.Json(new { success = false, message = "Windows Service could not be restart" });
            else
                return this.Json(new { success = true, message = "true" });
        }
        public bool ServiceExists(string ServiceName)
        {
            return ServiceController.GetServices().Any(serviceController => serviceController.ServiceName.Equals(ServiceName));
        }
        public static string RestartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                if (service.Status == ServiceControllerStatus.Running)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }

                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
                return "";
            }
            catch (Exception ex)
            {              
                return ex.Message + ex.StackTrace;
            }
        } 
        #endregion
    }
}
