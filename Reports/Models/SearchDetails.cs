using System;
using System.Collections.Generic;
using System.Text;

namespace Reports.DAL
{
    public class SearchDetails
    {
        public int Category { get; set; }
        public int Status { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

    }

    public class FilterData
    {
        public int Category { get; set; }
        public int Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
