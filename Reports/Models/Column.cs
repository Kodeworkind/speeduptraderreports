﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reports.DAL
{
    public class Column
    {
        public string Data { get; set; }
        public string Name { get; set; }
    }
}
