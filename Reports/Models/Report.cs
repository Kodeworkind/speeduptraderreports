using System;
using System.Collections.Generic;
using System.Text;

namespace Reports.DAL
{
    public class ReportDetails
    {

        public int TotalCount { get; set; }
        public int SuccessCount { get; set; }
        //public int TotalTradeCount { get; set; }
        //public int SuccessTradeCount { get; set; }
        public int InitiatedCount { get; set; }
        public int CreatedCount { get; set; }
        public int WaitingCount { get; set; }
        public int FailureCount { get; set; }

        public int PaymentFailureCount { get; set; }
        public List<Report> lstReport { get; set; }


    }
    public class Report
    {       
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string EmailType { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }

        public string UserName { get; set; }
        public string CsvName { get; set; }
        public string StatusName { get; set; }
    }
    public class Status
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }

    public class FileStatusModel
    {
        public List<Status> ListFileStatus { get; set; }
    }

    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
    public class Service
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
    public enum MainCategory
    {
        Pending_Emails = 1,
        Users = 2,
        Account = 3,
        Rms = 4
    }
}
