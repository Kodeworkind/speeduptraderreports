﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reports.DAL
{
    public class JData
    {
       public List<Column> columns = new List<Column>();

       public List<Report> data = new List<Report>();
    }
}
