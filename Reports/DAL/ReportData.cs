using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Reports.DAL
{
    public class ReportData
    {
        private string connectionString;
        private readonly IConfiguration _iConfiguration;

        public ReportData(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:SpeedUpTraderDatabase");
            _iConfiguration = configuration;
        }

        public ReportDetails GetCounts(SearchDetails searchDetails)
        {
            ReportDetails countDetails = new ReportDetails();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {

                    connection.Open();
                    countDetails = SqlMapper.Query<ReportDetails>(connection, "USP_GET_ALL_COUNTS",
                    new
                    {
                        fromDate = searchDetails.FromDate,
                        toDate = searchDetails.ToDate,
                        category = searchDetails.Category              
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    connection.Close();                    
                }
            }
            catch(Exception ex)
            { 

            }
            return countDetails;

        } 
        
        public List<Report> GetReportData(SearchDetails searchDetails)
        {
            List<Report> reports = new List<Report>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    reports = SqlMapper.Query<Report>(connection, "USP_GET_ALL_REPORT_DETAILS",
                    new
                    {
                        fromDate = searchDetails.FromDate,
                        toDate = searchDetails.ToDate,
                        status = searchDetails.Status,
                        category = searchDetails.Category
                    }, commandType: CommandType.StoredProcedure).AsList();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return reports;

        }

        public List<Status> GetFileStatus()
        {
            List<Status> statuses = new List<Status>();
            try
            {
                using(var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    statuses = SqlMapper.Query<Status>(connection, "USP_GET_FILE_STATUS", commandType: CommandType.StoredProcedure).AsList();
                    connection.Close();
                }
            }
            catch
            {

            }
            return statuses;

        }

        public User AuthenticateUser(string username, string password)
        {
            User user = new User();

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                user = SqlMapper.Query<User>(connection, "USP_AUTHENTICATE_USER_REPORTS", new { username, password }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                connection.Close();
            }

            return user;
        }
        public List<Service> GetServiceNames()
        {
            List<Service> services = new List<Service>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    services = SqlMapper.Query<Service>(connection, "USP_GET_RITHMIC_WINDOWS_SERVICES", commandType: CommandType.StoredProcedure).AsList();
                    connection.Close();
                }
            }
            catch
            {

            }
            return services;
        }
    }
}
