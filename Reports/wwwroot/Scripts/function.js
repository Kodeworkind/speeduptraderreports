﻿
$(window).on('resize', function () {
    var win = $(this); //this = window
    if (win.width() <= 766) {

        var General =
        {
            "general": [
                {

                    "basic": [
                        "Minimum 15 trading days",
                        "Profit target $1,500",
                        "3 contracts maximum",
                        "Daily loss limit $500",
                        "Maximum Drawdown $1500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events.",
                        "Start over at any time for $100"
                    ]
                },
                {
                    "pro": [
                        "Minimum 15 trading days",
                        "Profit target $3,000",
                        "6 contracts maximum",
                        "Daily loss limit $1250",
                        "Maximum Drawdown $2500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events.",
                        "Start over at any time for $100"
                    ]
                },
                {
                    "business": [
                        "Minimum 15 trading days",
                        "Profit target $6,000",
                        "12 contracts maximum",
                        "Daily loss limit $2500",
                        "Maximum Drawdown $3500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events.",
                        "Start over at any time for $100"
                    ]
                }
            ]

        }

        var expressdata = {
            "Express": [
                {
                    "basic": [
                        "Profit target $1,500",
                        "3 contracts maximum",
                        "Daily loss limit $500",
                        "Maximum Drawdown $1500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events."
                    ]
                },
                {
                    "pro": [
                        "Profit target $3,000",
                        "6 contracts maximum",
                        "Daily loss limit $1250",
                        "Maximum Drawdown $2500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events."
                    ]
                },
                {
                    "business": [
                        "Profit target $6,000",
                        "12 contracts maximum",
                        "Daily loss limit $2500",
                        "Maximum Drawdown $3500",
                        "Meet The Consistency Rule",
                        "Must close positions by 4:10 P.M. Eastern U.S. Time",
                        "May not trade through major news events."
                    ]
                }
            ]
        }

        for (var i in General.general) {
            for (var j in General.general[i].basic) {
                var x = General.general[i].basic[j];
                var li = $("#GeneralFunding .plans__item-basic .features-lists li")[j];
                $(li).text(x);
            }
        }

        for (var i in General.general) {
            for (var j in General.general[i].pro) {
                var x = General.general[i].pro[j];
                var li = $("#GeneralFunding .plans__item-pro .features-lists li")[j];
                $(li).text(x);
            }
        }

        for (var i in General.general) {
            for (var j in General.general[i].business) {
                var x = General.general[i].business[j];
                var li = $("#GeneralFunding .plans__item-business .features-lists li")[j];
                $(li).text(x);
            }
        }

        for (var i in expressdata.Express) {
            for (var j in expressdata.Express[i].basic) {
                var x = expressdata.Express[i].basic[j];
                var li = $("#ExpressFunding .plans__item-basic .features-lists li")[j];
                $(li).text(x);
            }
        }

        for (var i in expressdata.Express) {
            for (var j in expressdata.Express[i].pro) {
                var x = expressdata.Express[i].pro[j];
                var li = $("#ExpressFunding .plans__item-pro .features-lists li")[j];
                $(li).text(x);
            }
        }

        for (var i in expressdata.Express) {
            for (var j in expressdata.Express[i].business) {
                var x = expressdata.Express[i].business[j];
                var li = $("#ExpressFunding .plans__item-business .features-lists li")[j];
                //console.log(li);
                $(li).text(x);
            }
        }
        $('.resposive-remove .col-xs-6').addClass('col-xs-12').removeClass('col-xs-6');

    }
}).resize();






$(window).on('resize', function () {
    //alert();
    var win = $(this); //this = window

    if (win.width() == 768) {


        $(function () {
            //$('#checkbox').change(function () {
            //    setInterval(function () {
            //        moveRight();
            //    }, 4000);
            //});

            var slideCount = $('#slider .plans_wrapper .plans-tablet').length;
            var slideWidth = $('#slider').width();
            var slideHeight = $('.reference-height-div').height();
            var sliderUlWidth = slideCount * slideWidth;
            //alert(slideWidth);
            $('#slider').css({
                width: slideWidth,
                height: '479px'
            });

            $('#slider .plans_wrapper ').css({
                width: sliderUlWidth,
                marginLeft: -slideWidth
            });

            $('#slider .plans_wrapper .plans-tablet:last-child').prependTo('#slider .plans_wrapper ');

            function moveLeft() {
                $('#slider .plans_wrapper').animate({
                    left: +slideWidth
                }, 200, function () {
                    $('#slider .plans_wrapper .plans-tablet:last-child').prependTo('#slider .plans_wrapper');
                    $('#slider .plans_wrapper').css('left', '');
                });
            };

            function moveRight() {
                $('#slider .plans_wrapper').animate({
                    left: -slideWidth
                }, 200, function () {
                    $('#slider .plans_wrapper .plans-tablet:first-child').appendTo('#slider .plans_wrapper');
                    $('#slider .plans_wrapper').css('left', '');
                });
            };

            $('a.control_prev').click(function () {
                moveLeft();
            });

            $('a.control_next').click(function () {
                moveRight();
            });
        });


    }
}).resize();



$('.nav-pills > li').click(function () {

    if ($(window).width() == 768) {


        $(function () {
            //$('#checkbox').change(function () {
            //    setInterval(function () {
            //        moveRight();
            //    }, 3000);
            //});

            var slideCount = $('#sliders .plans_wrapper .plans-tablet').length;
            var slideWidth = $('#slider').width();
            var slideHeight = $('.reference-height-div').height();
            var sliderUlWidth = slideCount * slideWidth;
            //alert(slideWidth);
            $('#sliders').css({
                width: slideWidth,
                height: '409px'
            });

            $('#sliders .plans_wrapper ').css({
                width: sliderUlWidth,
                marginLeft: -slideWidth
            });

            $('#sliders .plans_wrapper .plans-tablet:last-child').prependTo('#sliders .plans_wrapper ');

            function moveLeft() {
                $('#sliders .plans_wrapper').animate({
                    left: +slideWidth
                }, 200, function () {
                    $('#sliders .plans_wrapper .plans-tablet:last-child').prependTo('#sliders .plans_wrapper');
                    $('#sliders .plans_wrapper').css('left', '');
                });
            };

            function moveRight() {
                $('#sliders .plans_wrapper').animate({
                    left: -slideWidth
                }, 200, function () {
                    $('#sliders .plans_wrapper .plans-tablet:first-child').appendTo('#sliders .plans_wrapper');
                    $('#sliders .plans_wrapper').css('left', '');
                });
            };

            $('a.control_prev').click(function () {
                moveLeft();
            });

            $('a.control_next').click(function () {
                moveRight();
            });
        });


    }
});

